import style from './UserList.module.css';
import UsersListFilters from './UsersListFilters';
import UsersListRows from './UsersListRows';
import { useFilters } from '../lib/hooks/useFilters';
import UserListPagination from './UsersListPagination';
import { useUsers } from '../lib/hooks/useUsers';
import { useState } from 'react';
import { USER_FORMS } from '../constants/userForms';
import Button from './buttons/Button';
import UserCreateForm from './user-forms/UserCreateForm';

const User = () => {
	const { currentForm, setFilterForm, setCreateForm } = useForm();

	const {
		filters,
		setSearch,
		setSortBy,
		setOnlyActive,
		setPage,
		setItemsPerPage
	} = useFilters();

	const { users, totalPages, error, loading } = useUsers(filters);

	return (
		<div className={style.list}>
			<h1 className={style.title}>Listado de usuarios</h1>

			{currentForm === USER_FORMS.FILTERS ? (
				<UsersListFilters
					search={filters.search}
					onlyActive={filters.onlyActive}
					sortBy={filters.sortBy}
					setSearch={setSearch}
					setSortBy={setSortBy}
					setOnlyActive={setOnlyActive}
					slot={<Button onClick={setCreateForm}>Añadir usuario</Button>}
				/>
			) : (
				<UserCreateForm onClose={setFilterForm} />
			)}

			<UsersListRows users={users} error={error} loading={loading} />
			<UserListPagination
				page={filters.page}
				itemsPerPage={filters.itemsPerPage}
				setPage={setPage}
				setitemsPerPage={setItemsPerPage}
				totalPages={totalPages}
			/>
		</div>
	);
};

const useForm = () => {
	const [currentForm, setCurrentForm] = useState(USER_FORMS.FILTERS);

	const setFilterForm = () => setCurrentForm(USER_FORMS.FILTERS);
	const setCreateForm = () => setCurrentForm(USER_FORMS.CREATE);
	const setEditForm = () => setCurrentForm(USER_FORMS.EDIT);
	const setDeleteForm = () => setCurrentForm(USER_FORMS.DELETE);

	return {
		currentForm,
		setFilterForm,
		setCreateForm,
		setEditForm,
		setDeleteForm
	};
};

export default User;

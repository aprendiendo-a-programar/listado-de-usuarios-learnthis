import { SORT_OPTIONS } from '../constants/sortOptions';
import InputCheckbox from './forms/InputCheckbox';
import InputSearch from './forms/InputSearch';
import Select from './forms/Select';
import style from './UsersListFilters.module.css';

const UsersListFilters = ({
	search,
	setSearch,
	onlyActive,
	setOnlyActive,
	sortBy,
	setSortBy,
	slot
}) => {
	return (
		<div className={style.form}>
			<div className={style.row}>
				<InputSearch
					placeholder="Buscar..."
					value={search}
					onChange={(e) => setSearch(e.target.value)}
				/>

				<Select
					value={sortBy}
					onChange={(e) => setSortBy(Number(e.target.value))}
				>
					<option value={SORT_OPTIONS.DEFAULT}>Por defecto</option>
					<option value={SORT_OPTIONS.NAME}>Por nombre</option>
					<option value={SORT_OPTIONS.ROLE}>Por rol</option>
					{!onlyActive && <option value={SORT_OPTIONS.ACTIVE}>Por activos</option>}
				</Select>
			</div>

			<div className={style.row}>
				<div className={style.active}>
					<InputCheckbox
						className={style.checkbox}
						checked={onlyActive}
						onChange={(e) => setOnlyActive(e.target.checked)}
					/>
					<span>Mostrar sólo activos</span>
				</div>
				{slot}
			</div>
		</div>
	);
};

export default UsersListFilters;
